package com.example.databaseapp.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.databaseapp.R;
import com.example.databaseapp.interfaces.ActivitiesFragmentsCommunication;
import com.example.databaseapp.models.Book;

public class BookDetailsFragment extends Fragment {
    public static final String TAG_DETAILS = "TAG_DETAILS";
    private Book book;
    private ActivitiesFragmentsCommunication fragmentCommunication;

    public BookDetailsFragment(Book book) {
        this.book = book;
    }

    public static BookDetailsFragment newInstance(Book book) {

        Bundle args = new Bundle();

        BookDetailsFragment fragment = new BookDetailsFragment(book);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_book_details, container, false);

        return view;

    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        if (context instanceof ActivitiesFragmentsCommunication) {
            fragmentCommunication = (ActivitiesFragmentsCommunication) context;
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


//        view.findViewById(R.id.btn_login).setOnClickListener(new View.OnClickListener(){
//
//            @Override
//            public void onClick(View v) {
//
//                goToLogin();
//            }
//        });

         TextView title;
         TextView author;
         TextView description;

        title= view.findViewById(R.id.txt_title);
        author=view.findViewById(R.id.txt_author);
        description=view.findViewById(R.id.txt_description);
        title.setText(book.getTitle());
        author.setText(book.getAuthor());
        description.setText(book.getDescription());
    }
}
