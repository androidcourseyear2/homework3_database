package com.example.databaseapp.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.databaseapp.R;
import com.example.databaseapp.adapters.BookAdapter;
import com.example.databaseapp.helpers.Utils;
import com.example.databaseapp.interfaces.ActivitiesFragmentsCommunication;
import com.example.databaseapp.interfaces.OnBookItemClick;
import com.example.databaseapp.models.Book;
import com.example.databaseapp.models.BookDatabase;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class BookAppFragment extends Fragment {
    public static final String TAG_BOOK = "TAG_BOOK";

    private ActivitiesFragmentsCommunication fragmentCommunication;

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference();

    ArrayList<Book> books = new ArrayList<Book>();
    ArrayList<BookDatabase> booksArray = new ArrayList<BookDatabase>();

    RecyclerView bookList;

    public boolean checkData=false;


    BookAdapter bookAdapter = new BookAdapter(books, new OnBookItemClick() {
        @Override
        public void onClick(Book book) {
            if (fragmentCommunication != null) {
                //fragmentCommunication.onReplaceFragment(TAG_DETAILS);
                fragmentCommunication.onReplaceSecondFragment(book);
                Toast.makeText(getContext(), "Go fragment 2", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onDeleteClick(Book book) {

            Toast.makeText(getContext(), "Delete Book ", Toast.LENGTH_SHORT).show();
            deleteDataFromDatabase(book);

        }
    });

    public static BookAppFragment newInstance() {

        Bundle args = new Bundle();

        BookAppFragment fragment = new BookAppFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_book_app, container, false);


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        bookList = (RecyclerView) view.findViewById(R.id.book_list);
        bookList.setLayoutManager(linearLayoutManager);
        getDataFromDatabase();
        bookList.setAdapter(bookAdapter);
        return view;
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        if (context instanceof ActivitiesFragmentsCommunication) {
            fragmentCommunication = (ActivitiesFragmentsCommunication) context;
        }
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.btn_book).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                EditText titleEdtText = (EditText) view.findViewById(R.id.edt_title);
                String title = titleEdtText.getText().toString();
                if (!Utils.isValidTitleAndAuthor(title)) {
                    titleEdtText.setError("Invalid input");
                    return;
                } else {
                    titleEdtText.setError(null);
                }
                EditText authorEdtText = (EditText) view.findViewById(R.id.edt_author);
                String author = authorEdtText.getText().toString();
                if (!Utils.isValidTitleAndAuthor(author)) {
                    authorEdtText.setError("Invalid input");
                    return;
                } else {
                    authorEdtText.setError(null);
                }
                EditText descriptionEdtText = (EditText) view.findViewById(R.id.edt_description);
                String description = descriptionEdtText.getText().toString();
                if (!Utils.isValidDescription(description)) {
                    descriptionEdtText.setError("Invalid input");
                    return;
                } else {
                    descriptionEdtText.setError(null);
                }

                if(Utils.isDataExist(title,author,booksArray)=="invalid")
                {
                    writeNewBook(title, author, description);
                    getDataFromDatabase();
                    Toast.makeText(getContext(), "Add Book", Toast.LENGTH_SHORT).show();



                }
                else
                {
                    Toast.makeText(getContext(), "This book exist!", Toast.LENGTH_SHORT).show();
                    String bookId=Utils.isDataExist(title,author,booksArray);
                    updateBook(bookId,description);
                    getDataFromDatabase();


                }

                titleEdtText.setText("");
                authorEdtText.setText("");
                descriptionEdtText.setText("");
            }
        });

    }

    public void writeNewBook(String title, String author, String description) {
        Book book = new Book(title, author, description);
        myRef.child("books").push().setValue(book);
    }

    public void deleteDataFromDatabase(Book book) {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
        Query applesQuery = ref.child("books").orderByChild("title").equalTo(book.getTitle());

        applesQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                    appleSnapshot.getRef().removeValue();
                    getDataFromDatabase();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void getDataFromDatabase() {
        books.clear();
        booksArray.clear();
        DatabaseReference booksRef = myRef.child("books");
        ValueEventListener eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    Book data = ds.getValue(Book.class);
                    Book book = new Book(ds.getValue(Book.class).getTitle(), data.getAuthor(), data.getDescription());
                    books.add(book);
                    String key=ds.getKey();
                    BookDatabase bookFirebase = new BookDatabase(ds.getValue(Book.class).getTitle(), data.getAuthor(), data.getDescription(),key);
                    booksArray.add(bookFirebase);



                    bookAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        };
        booksRef.addListenerForSingleValueEvent(eventListener);
    }

    public void updateBook(String id,String description){
        myRef.child("books").child(id).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                dataSnapshot.getRef().child("description").setValue(description);


            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


}
