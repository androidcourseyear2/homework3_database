package com.example.databaseapp.interfaces;

import com.example.databaseapp.models.Book;

public interface ActivitiesFragmentsCommunication {

    void onReplaceFragment(String TAG);
    void onReplaceSecondFragment(Book book);
}
