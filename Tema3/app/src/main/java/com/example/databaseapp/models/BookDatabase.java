package com.example.databaseapp.models;

import androidx.annotation.NonNull;

public class BookDatabase {

    private String title;
    private String author;
    private String id;

    public BookDatabase(String title, String author, String description) {
        this.title = title;
        this.author = author;
        this.description = description;
    }

    public BookDatabase(String title, String author, String description, String id) {
        this.title = title;
        this.author = author;
        this.description = description;
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public BookDatabase() {

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    private String description;

    @NonNull
    @Override
    public String toString() {
        return title + " " + author + " " + description;
    }
}
