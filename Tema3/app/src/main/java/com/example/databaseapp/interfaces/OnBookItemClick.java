package com.example.databaseapp.interfaces;

import com.example.databaseapp.models.Book;

public interface OnBookItemClick {

    void onClick(Book book);
    void onDeleteClick(Book book);
}
