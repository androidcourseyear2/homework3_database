package com.example.databaseapp.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.databaseapp.R;
import com.example.databaseapp.interfaces.OnBookItemClick;
import com.example.databaseapp.models.Book;

import java.util.ArrayList;

public class BookAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ArrayList<Book> books;
    private OnBookItemClick onBookItemClick;


    public BookAdapter(ArrayList<Book> books, OnBookItemClick onBookItemClick) {
        this.books = books;
        this.onBookItemClick = onBookItemClick;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.book_item_cell, parent, false);
        BookViewHolder bookViewHolder = new BookViewHolder(view);
        return bookViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof BookViewHolder) {
            Book book = (Book) books.get(position);
            ((BookViewHolder) holder).bind(book);
        }

    }

    @Override
    public int getItemCount() {
        return this.books.size();
    }

    class BookViewHolder extends RecyclerView.ViewHolder {
        private TextView title;
        private TextView author;
        private TextView description;
        private Button delete;

        private View view;

        //constructor
        BookViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.txt_title);
            author = view.findViewById((R.id.txt_author));
            description = view.findViewById((R.id.txt_description));
            delete=view.findViewById(R.id.btn_delete);
            this.view = view;
        }


        void bind(Book book) {
            title.setText(book.getTitle());
            author.setText(book.getAuthor());
            description.setText(book.getDescription());
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onBookItemClick != null) {
                        onBookItemClick.onClick(book);
                    }
                }
            });
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(onBookItemClick!=null){
                        onBookItemClick.onDeleteClick(book);
                    }
                }
            });
        }

    }
}
