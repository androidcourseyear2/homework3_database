package com.example.databaseapp.helpers;

import android.text.TextUtils;

import com.example.databaseapp.models.BookDatabase;

import java.util.ArrayList;

public class Utils {
    public static boolean isValidTitleAndAuthor(String input) {
        return !TextUtils.isEmpty(input) && input.length() <=100;
    }
    public static boolean isValidDescription(String input) {
        return !TextUtils.isEmpty(input) && input.length() <=1000;
    }

    public static String isDataExist(String title,String author, ArrayList<BookDatabase> books) {
        for (int i = 0; i < books.size(); i++) {
            if (books.get(i).getTitle().equals(title)&& books.get(i).getAuthor().equals(author)) {

                return books.get(i).getId();
            }
        }
        return "invalid";
    }
}
