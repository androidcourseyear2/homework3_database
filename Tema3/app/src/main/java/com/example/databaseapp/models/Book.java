package com.example.databaseapp.models;

import androidx.annotation.NonNull;

public class Book {

    private String title;
    private String author;

    public Book(String title, String author, String description) {
        this.title = title;
        this.author = author;
        this.description = description;
    }

    public Book()
    {

    }
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    private String description;

    @NonNull
    @Override
    public String toString() {
        return title+" "+author+" "+description;
    }
}
