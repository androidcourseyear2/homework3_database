package com.example.databaseapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;

import com.example.databaseapp.fragments.BookAppFragment;
import com.example.databaseapp.fragments.BookDetailsFragment;
import com.example.databaseapp.interfaces.ActivitiesFragmentsCommunication;
import com.example.databaseapp.models.Book;

import static com.example.databaseapp.fragments.BookAppFragment.TAG_BOOK;
import static com.example.databaseapp.fragments.BookDetailsFragment.TAG_DETAILS;

public class MainActivity extends AppCompatActivity implements ActivitiesFragmentsCommunication {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        onAddBookFragment();
    }

    private void onAddBookFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.frame_layout, BookAppFragment.newInstance(), TAG_BOOK);

        fragmentTransaction.commit();
    }

    @Override
    public void onReplaceFragment(String TAG) {

//        FragmentManager fragmentManager = getSupportFragmentManager();
//        Fragment fragment;
//
//        switch (TAG) {
//            case TAG_BOOK: {
//                fragment = BookAppFragment.newInstance();
//                break;
//            }
//            case TAG_DETAILS: {
//                fragment = BookDetailsFragment.newInstance();
//                break;
//            }
//
//            default:
//                return;
//        }
//        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//        fragmentTransaction.replace(R.id.frame_layout, fragment, TAG);
//
//        fragmentTransaction.commit();
    }

    @Override
    public void onReplaceSecondFragment(Book book) {


        String tag=TAG_DETAILS;
        FragmentManager fragmentManager=getSupportFragmentManager();
        FragmentTransaction transaction= fragmentManager.beginTransaction();
        FragmentTransaction addTransaction= transaction.replace(R.id.frame_layout, BookDetailsFragment.newInstance(book),tag);

       addTransaction.addToBackStack(tag);

        addTransaction.commit();
    }
}